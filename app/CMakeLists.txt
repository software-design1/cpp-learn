cmake_minimum_required(VERSION 3.22)
set(CMAKE_CXX_STANDARD 17)
project(app)

add_subdirectory(ioc)

add_library(rmapp)
target_sources(rmapp PUBLIC src/app.cpp)
target_link_libraries(rmapp PUBLIC ioc)
