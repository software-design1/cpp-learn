#pragma once

#include <memory>

class Module;
class Context;

class Injectable
{
public:
    /**
     * call all injection in init
     */
    virtual void init(){};

    std::shared_ptr<Context> context;
};

class Bean : public Injectable
{
};

class Component : public Bean
{
};

class Service : public Component
{
};