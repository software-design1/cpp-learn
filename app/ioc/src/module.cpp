#include "module.hpp"

void Module::run()
{
    this->init();
    this->injectAll();
}

void Module::injectAll()
{

    /**
     * init sub modules
     */
    for (auto module : this->modules)
    {
        module->init();
    }

    /**
     * inject dependency for injectables
     */
    for (auto injectable : this->injectables)
    {
        injectable.second->init();
    }
}