#pragma once

#include <unordered_map>
#include <vector>
#include <string>
#include <memory>
#include <type_traits>
#include <stdexcept>
#include "interface.hpp"
#include "utils.hpp"

class Context
{
public:
    template <class T>
    std::shared_ptr<T> getInjectable(std::string) {
        return nullptr;
    }
};

class Module : public std::enable_shared_from_this<Module>, public virtual Context
{
public:
    template <class T>
    std::shared_ptr<T> getInjectable(std::string name)
    {
        if (std::is_base_of_v<Injectable, T>)
        {
            std::shared_ptr<T> injectable = nullptr;
            auto search = this->injectables.find(name);

            if (search != this->injectables.end())
            {
                return dynamic_pointer_cast_throwable<T, Injectable>(search->second);
            }

            for (auto module : this->modules)
            {
                injectable = module->getInjectableForModules<T>(name);
                if (injectable != nullptr)
                {
                    return injectable;
                }
            }

            throw std::runtime_error("No Injectable with name: " + name);
        }
    }

    /**
     * get injectable only between modules, dont use this function in injectable init
     */
    template <class T>
    std::shared_ptr<T> getInjectableForModules(std::string name)
    {
        std::shared_ptr<T> injectable = nullptr;
        auto search = this->injectables.find(name);

        if (search != this->injectables.end())
        {
            return dynamic_pointer_cast_throwable<T, Injectable>(search->second);
        }

        for (auto module : this->modules)
        {
            injectable = module->getInjectable<T>(name);
            if (injectable != nullptr)
            {
                return injectable;
            }
        }

        return injectable;
    }

    /**
     * call only to start main module
     */
    void run();

    void setParentModule(std::shared_ptr<Module> m)
    {
        this->parentModule = m;
    }

protected:
    /**
     * add injectable in module context, call in in initModule function override
     */
    template <class T>
    void addInjectable(std::string name)
    {
        if (std::is_base_of_v<Injectable, T>)
        {
            auto injectable = static_pointer_cast_throwable<Injectable, T>(std::make_shared<T>());
            injectable->context = static_pointer_cast_throwable<Context, Module>(shared_from_this());
            this->injectables[name] = injectable;
        }
    }

    template <class M>
    void addSubmodule()
    {
        if (std::is_base_of_v<Module, M>)
        {
            auto module = static_pointer_cast_throwable<Module, M>(std::make_shared<M>());
            module->setParentModule(shared_from_this());
            this->modules.push_back(module);
        }
    }

    /**
     * pure virtual, define injectable of this module here
     */
    virtual void init() = 0;

private:
    /**
     * inject dependency for all element in context
     */
    void injectAll();

    std::unordered_map<std::string, std::shared_ptr<Injectable>> injectables;
    std::vector<std::shared_ptr<Module>> modules;

    std::shared_ptr<Module> parentModule;
};