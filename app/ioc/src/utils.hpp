#pragma once

#include <memory>

template<class T, class M>
std::shared_ptr<T> static_pointer_cast_throwable(std::shared_ptr<M> ptr) {
    auto castedPtr = std::static_pointer_cast<T>(ptr);
    if(castedPtr == nullptr) {
        throw "failed cast";
    }

    return castedPtr;
}

template<class T, class M>
std::shared_ptr<T> dynamic_pointer_cast_throwable(std::shared_ptr<M> ptr) {
    auto castedPtr = std::dynamic_pointer_cast<T>(ptr);
    if(castedPtr == nullptr) {
        throw "failed cast";
    }

    return castedPtr;
}