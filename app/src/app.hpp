#pragma once

#include <memory>
#include <type_traits>

#include "../ioc/src/module.hpp"
#include "../ioc/src/utils.hpp"

class App {        
    public:
        template <class M>
        void bootstrapModule() {
            if(std::is_base_of_v<Module, M>) {
                this->bootstrap = static_pointer_cast_throwable<Module, M>(std::make_shared<M>());
                this->bootstrap->run();
            }
        }
    private:
        std::shared_ptr<Module> bootstrap;
};