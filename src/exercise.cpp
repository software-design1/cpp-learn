#include <iostream>
#include <string>
#include <algorithm>
#include <cmath>
#include <numeric>

void vectorPrinter(std::vector<int> arr)
{
    std::cout << "[";
    std::for_each(arr.begin(), arr.end(), [](auto e)
                  { std::cout << e << " "; });
    std::cout << "]" << std::endl;
}

/**
 * Broken Bridge
 */
bool isSafeBridge(std::string bridge)
{
    // auto result = std::find(bridge.begin(), bridge.end(), ' ');
    // return result == bridge.end();
    return std::accumulate(bridge.begin(), bridge.end(), true, [](bool acc, char e)
                           {
        if(e == ' ') {
            acc = false;
        }

        return acc; });
}

/**
 * no Odds
 */
std::vector<int> noOdds(std::vector<int> arr)
{
    std::vector<int> result;
    std::copy_if(arr.begin(), arr.end(), std::inserter(result, result.end()), [](int element)
                 { return element % 2 == 0; });

    std::for_each(result.begin(), result.end(), [](auto e)
                  { std::cout << e << " "; });
    std::cout << std::endl;

    return result;
}

/**
 * hours in seconds
 */
int howManySeconds(int hours)
{
    return hours * 60 * 60;
}

/**
 * next number
 */
int addition(int a)
{
    return ++a;
}

/**
 *
 */
std::vector<int> sortNumsAscending(std::vector<int> arr)
{
    if (arr.empty())
    {
        return {};
    }
    std::sort(arr.begin(), arr.end());
    vectorPrinter(arr);
    return arr;
}

/**
 * Travelling Salesman Problem
 */
long int paths(int n)
{
    if (n < 0)
        throw std::logic_error("undefined");
    return n == 0 ? 1 : n * paths(n - 1);
}

/**
 * Last Digit Ultimate
 */
bool lastDig(int a, int b, int c)
{
    int value = a * b;

    return value % 10 == c % 10;
}

/**
 * Count Syllables
 */
int numberSyllables(std::string word)
{
    return std::count(word.begin(), word.end(), '-') + 1;
}

/**
 * Burrrrrrrp
 */
std::string longBurp(int num)
{
    std::string burp = "Bu";

    for(int i = 0; i < num; i++) {
        burp += 'r';
    }

    burp += "p";
    return burp;
}

int main()
{
    // std::cout << (isSafeBridge("#####") ? "ok" : "no") << std::endl;
    // std::cout << (isSafeBridge("# ") ? "ok" : "no") << std::endl;
    // std::cout << (isSafeBridge("## ##") ? "ok" : "no") << std::endl;

    // noOdds({1, 2, 3, 4, 5, 6, 7, 8});
    // noOdds({43, 65, 23, 89, 53, 9, 6});
    // noOdds({718, 991, 449, 644, 380, 440});

    // std::cout << addition(0) << std::endl;
    // std::cout << addition(9) << std::endl;
    // std::cout << addition(-3) << std::endl;

    // sortNumsAscending({ 1, 2, 10, 50, 5 });
    // sortNumsAscending({ 80, 29, 4, -95, -24, 85 });
    // sortNumsAscending({});

    // std::cout << paths(4) << std::endl;
    // std::cout << paths(1) << std::endl;
    // std::cout << paths(9) << std::endl;

    // std::cout << lastDig(25, 21, 125) << std::endl;
    // std::cout << lastDig(55, 226, 5190) << std::endl;
    // std::cout << lastDig(12, 215, 2142) << std::endl;

    // std::cout << numberSyllables("buf-fet") << std::endl;
    // std::cout << numberSyllables("beau-ti-ful") << std::endl;
    // std::cout << numberSyllables("mon-u-men-tal") << std::endl;
    // std::cout << numberSyllables("on-o-mat-o-poe-ia") << std::endl;

    std::cout << longBurp(3) << std::endl;
    std::cout << longBurp(5) << std::endl;
    std::cout << longBurp(9) << std::endl;

    // return std::cin.get();
    return 0;
}