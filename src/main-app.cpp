#include <iostream>
#include "../app/ioc/src/module.hpp"
#include "../app/src/app.hpp"

class MyBean : public Bean
{
public:
    MyBean()
    {
        std::cout << "MyBean Construct" << std::endl;
    }

    void print()
    {
        std::cout << "calling injectable function" << std::endl;
    }
};

class MyService : public Service
{
public:
    MyService()
    {
        std::cout << "MyService Construct" << std::endl;
    }

    void init()
    {
        auto myBean = this->context->getInjectable<MyBean>("MyBean");
        myBean->print();
    }
};

class MySubModule : public Module
{
private:
    void init()
    {
        std::cout << "my sub module" << std::endl;
        this->addInjectable<MyBean>("MyBean");
    }
};

class MyModule : public Module
{
private:
    void init()
    {
        std::cout << "my module" << std::endl;
        this->addInjectable<MyService>("MyService");
        this->addSubmodule<MySubModule>();
    }
};

class MyApp : public App
{
public:
    MyApp()
    {
        std::cout << "App starter" << std::endl;
        this->bootstrapModule<MyModule>();
    }
};

int main()
{
    MyApp();
}