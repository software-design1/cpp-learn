#include <iostream>
#include <utility>
#include <string>

class RM {
    public:
        RM() {};
        mutable int attr = 0;
    private:
        const int attr2 = 5;
};

int main()
{
    std::string alfa = "alfa value";
    std::string beta;

    std::cout << "alfa: " << alfa << " -> "
              << "beta: " << beta << std::endl;

    beta = std::move(alfa);

    std::cout << "alfa: " << alfa << " -> "
              << "beta: " << beta << std::endl;

    std::string gamma = std::move(beta);

    std::cout << "alfa: " << alfa << " -> "
              << "beta: " << beta << " -> "
              << "gamma: " << gamma << std::endl;

    RM rm;
    const RM rm2;
    rm.attr = 5;
    rm2.attr = 7;

    return std::cin.get();
}