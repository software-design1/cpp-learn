#include <iostream>
#include "../app/ioc/src/module.hpp"

class MyBean : public Bean
{
public:
    MyBean()
    {
        std::cout << "MyBean Construct" << std::endl;
    }

    void print()
    {
        std::cout << "Hello" << std::endl;
    }
};

class MyService : public Service
{
public:
    MyService()
    {
        std::cout << "MyService Construct" << std::endl;
    }

    void init() {
        auto myBean = this->context->getInjectable<MyBean>("MyBean");
        myBean->print();
    }
};

class MyModule : public Module
{
public:
    void init()
    {
        std::cout << "hi" << std::endl;
        this->addInjectable<MyBean>("MyBean");
        this->addInjectable<MyService>("MyService");
    }
};

int main()
{
    // std::make_shared<MyModule>();
    MyModule().run();
}